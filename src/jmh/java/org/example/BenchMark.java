package org.example;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.example.map.ImprovedMap;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

public class BenchMark {
    @State(Scope.Benchmark)
    public static class States {
        public Map<Long, String> improvedMap = new ImprovedMap<>();
        public Map<Long, String> syncMap = Collections.synchronizedMap(new HashMap<>());
        public Map<Long, String> concurrentHashMap = new ConcurrentHashMap<>();
        public static Map<Long, String> dataMap = new HashMap<>();
        static {
            for (long i = 0; i < 100000; i++) {
                dataMap.put(i, "value" + i);
            }
        }
    }

    @Benchmark
    public void testImprovedMapPuts(States states) {
        test(states.improvedMap);
    }

    @Benchmark
    public void testSyncMapPuts(States states) {
        test(states.syncMap);
    }

    @Benchmark
    public void testConcurrentHashMapPuts(States states) {
        test(states.concurrentHashMap);
    }

    public void test(Map<Long, String> map) {
        for (Map.Entry<Long, String> entry : States.dataMap.entrySet()) {
            map.put(entry.getKey(), entry.getValue());
        }
    }
}
