package org.example;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class TestNotReleaseHarness implements MetricHarness {
    @Override
    public long timeTasks(int nThread, final Runnable task) throws InterruptedException {
        final CyclicBarrier startGate = new CyclicBarrier(nThread + 1);
        final CyclicBarrier endGate = new CyclicBarrier(nThread + 1);

        for (int i = 0; i < nThread; i++) {
            Thread t = new Thread() {
                public void run() {
                    try {
                        startGate.await();
                        try {
                            task.run();
                        } finally {
                            endGate.await();
                        }
                    } catch (InterruptedException | BrokenBarrierException ignored) {
                    }
                }
            };
            t.start();
        }

        long start = System.nanoTime();
        try {
            startGate.await();
            endGate.await();
            long end = System.nanoTime();
            return end - start;
        } catch (BrokenBarrierException ignored) {
            return -1;
        }
    }
}
