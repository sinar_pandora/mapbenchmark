package org.example;

import java.util.concurrent.*;

public class ImprovedForkJoinTestHarness implements MetricHarness {
    @Override
    public long timeTasks(int nThread, final Runnable task) throws InterruptedException {
        final CountDownLatch endGate = new CountDownLatch(nThread);
        final ExecutorService exec = Executors.newWorkStealingPool(2);

        long start = System.nanoTime();
        for (int i = 0; i < nThread; i++) {
            exec.execute(() -> {
                try {
                    task.run();
                } finally {
                    endGate.countDown();
                }
            });
        }

        try {
            endGate.await();
            long end = System.nanoTime();
            return end - start;
        } finally {
            exec.shutdownNow();
        }
    }
}
