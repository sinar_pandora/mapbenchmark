package org.example;

@FunctionalInterface
public interface MetricHarness {
    long timeTasks(int nThread, final Runnable task) throws InterruptedException;
}
