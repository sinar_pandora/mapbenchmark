package org.example;

import java.util.concurrent.*;

public class ImprovedTestHarness implements MetricHarness {
    @Override
    public long timeTasks(int nThread, final Runnable task) throws InterruptedException {
        final CyclicBarrier startGate = new CyclicBarrier(nThread + 1);
        final CyclicBarrier endGate = new CyclicBarrier(nThread + 1);
        final ExecutorService exec = Executors.newFixedThreadPool(nThread);

        for (int i = 0; i < nThread; i++) {
            exec.execute(() -> {
                try {
                    startGate.await();
                    try {
                        task.run();
                    } finally {
                        endGate.await();
                    }
                } catch (InterruptedException | BrokenBarrierException ignored) {
                }
            });
        }

        long start = System.nanoTime();
        try {
            startGate.await();
            endGate.await();
            long end = System.nanoTime();
            return end - start;
        } catch (BrokenBarrierException ignored) {
            return -1;
        } finally {
            exec.shutdownNow();
        }
    }
}
