package org.example;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.example.map.ImprovedMap;

public class TestRunner {
    private static final int NUM_THREADS = 20;
    private static final int WARM_UP_ROUNDS = 4;

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < WARM_UP_ROUNDS; i++) {
            test(new TestNotReleaseHarness());
            test(new ImprovedTestHarness());
            test(new TestHarness());
            test(new ImprovedForkJoinTestHarness());
        }
        System.out.println("=========== End of double warm-up ===========");
        test(new TestNotReleaseHarness());
        test(new ImprovedTestHarness());
        test(new TestHarness());
        test(new ImprovedForkJoinTestHarness());
    }

    @SuppressWarnings({"MismatchedQueryAndUpdateOfCollection"})
    public static void test(MetricHarness harness) throws InterruptedException {
        System.out.println("Using: " + harness.getClass().getSimpleName());
        // Prepare test data
        Map<Long, String> dataMap = new HashMap<>();
        for (long i = 0; i < 100000; i++) {
            dataMap.put(i, "value" + i);
        }
        System.out.println("Data map size: " + dataMap.size());
        System.out.println("Warm-up rounds: " + WARM_UP_ROUNDS);
        System.out.println("Threads: " + NUM_THREADS);
        System.out.println("Running warm-up...");
        // Warm-up:
        for (int i = 0; i < WARM_UP_ROUNDS; i++) {
            Map<Long, String> warmUpImprovedMap = new ImprovedMap<>();
            Map<Long, String> warmUpSyncMap = Collections.synchronizedMap(new HashMap<>());
            Map<Long, String> warmUpConcurrentHashMap = new ConcurrentHashMap<>();
            harness.timeTasks(NUM_THREADS, () -> {
                for (Map.Entry<Long, String> entry : dataMap.entrySet()) {
                    warmUpImprovedMap.put(entry.getKey(), entry.getValue());
                    warmUpImprovedMap.get(entry.getKey());
                    warmUpSyncMap.put(entry.getKey(), entry.getValue());
                    warmUpSyncMap.get(entry.getKey());
                    warmUpConcurrentHashMap.put(entry.getKey(), entry.getValue());
                    warmUpConcurrentHashMap.get(entry.getKey());
                }
            });
        }
        System.out.println("Warm-up finished");

        System.out.println("Start test...");
        Map<Long, String> improvedMap = new ImprovedMap<>();
        Map<Long, String> syncMap = Collections.synchronizedMap(new HashMap<>());
        Map<Long, String> concurrentHashMap = new ConcurrentHashMap<>();
        long improvedMapTime = harness.timeTasks(NUM_THREADS, () -> {
            for (Map.Entry<Long, String> entry : dataMap.entrySet()) {
                improvedMap.put(entry.getKey(), entry.getValue());
                improvedMap.get(entry.getKey());
            }
        });
        System.out.println("ImprovedMap:        " + improvedMapTime);
        long syncMapTime = harness.timeTasks(NUM_THREADS, () -> {
            for (Map.Entry<Long, String> entry : dataMap.entrySet()) {
                syncMap.put(entry.getKey(), entry.getValue());
                syncMap.get(entry.getKey());
            }
        });
        System.out.println("SyncMap:            " + syncMapTime);
        long concurrentHashMapTime = harness.timeTasks(NUM_THREADS, () -> {
            for (Map.Entry<Long, String> entry : dataMap.entrySet()) {
                concurrentHashMap.put(entry.getKey(), entry.getValue());
                concurrentHashMap.get(entry.getKey());
            }
        });
        System.out.println("ConcurrentHashMap:  " + concurrentHashMapTime);
    }
}
